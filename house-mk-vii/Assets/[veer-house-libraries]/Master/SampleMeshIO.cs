﻿/// File: SampleMeshIO.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolkit
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
	public class SampleMeshIO : MonoBehaviour
	{
		private const string SCRIPTNAME = "SampleMeshIO.cs: ";

        private Mesh mesh;
		#region MonoBehaviours
		void Start () {
            MeshFilter flt = this.gameObject.GetComponent<MeshFilter>();
            MeshRenderer rnd = this.gameObject.GetComponent<MeshRenderer>();

            // Reads in mesh
            string path = System.IO.Path.Combine(
                System.IO.Path.Combine(Application.dataPath, "[toolkit-mesh-manipulation]"),
                "capsule.obj");
            mesh = MeshIO.Read(path, rnd);
            Debug.Log(SCRIPTNAME + string.Format("Reading in mesh at '{0}'...", path));


            flt.mesh = mesh;
		}	

        void OnDrawGizmos()
        {
            if (mesh.vertices != null) {
                foreach (Vector3 vertex in mesh.vertices) {
                    Gizmos.DrawSphere(vertex, 0.01f);
                }
            }
        }
		#endregion
		
	}
}