﻿/// File: GenerateMesh.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolkit
{
    public static class MeshGeneration
    {
        private const string SCRIPTNAME = "GenerateMesh.cs: ";

        public static Mesh GenerateMesh(Vector3[] vertices)
        {
            Mesh output = new Mesh();

            // Generates triangles from vertices
            // NOTE: assumes vertices is a grid

            output.vertices = vertices;
            output.name = "GeneratedMesh";

            return output;
        }
    }
}