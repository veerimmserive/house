﻿/// File: MeshIO.cs
/// Description: 

using System.IO;
using System.Collections.Generic;
using UnityEngine;

namespace Toolkit
{
    public static class MeshIO
    {
        private const string SCRIPTNAME = "MeshIO.cs: ";
        private const string DIRECTORY = "[toolkit-mesh-manipulation]";

        /// <summary>
        /// Reads in a mesh from a specified (OBJ) file via the following:
        /// 0) Opens file to be read
        /// 1) Separates lines from file into lists
        /// 2) Obtains data from lines in lists
        /// 3) Returns mesh with obtained data
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        // TODO: generalise this to other file formats
        public static Mesh Read(string path, Renderer rnd = null)
        {
            Mesh output = new Mesh();

            // Checks if file in filename is a compatible OBJ file
            // TODO: use a file analyser
            Debug.Assert(string.Equals(
                Path.GetExtension(path).ToUpperInvariant(),
                ".obj".ToUpperInvariant()),
                SCRIPTNAME + "3D file format + " + Path.GetExtension(path).ToUpperInvariant() + " not supported.");

            // Gets all lines containing information
            // TODO: parse data in one step instead of going through string gymnastics
            List<string> linesVertex = new List<string>();
            List<string> linesVertexTexture = new List<string>();
            List<string> linesVertexNormal = new List<string>();
            List<string> linesVertexPoint = new List<string>();                      
            List<string> linesTriangle = new List<string>();
            List<string> linesMaterial = new List<string>();
            using (StreamReader f = new StreamReader(path)) {
                // Defines a dictionary of handlers of data from file
                // TODO: implement handlers
                Dictionary<string, System.Action<string>> handlerLine = new Dictionary<string, System.Action<string>>() {
                        { "v",
                            (line) => { linesVertex.Add(line); } },
                        { "vt",
                            (line) => { linesVertexTexture.Add(line); } },
                        { "vn",
                            (line) => { linesVertexNormal.Add(line); } },
                        { "vp",
                            (line) => { linesVertexPoint.Add(line); } },                       
                        { "cstype",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle curve/surface type (cstype) information."); } },
                        { "deg",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle degree (deg) information."); } },
                        { "bmat",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle basis matrix (bmat) information."); } },
                        { "step",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle step size (step) information."); } },
                        { "p",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle point (p) information."); } },
                        { "l",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle line (l) information."); } },
                        { "f",
                            (line) => { linesTriangle.Add(line); } },
                        { "curv",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle curve (curv) information."); } },
                        { "curv2",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle 2D curve (curv2) information."); } },
                        { "surf",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle surface (surf) information."); } },
                        { "parm",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle parameter values (parm) information."); } },
                        { "trim",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle outer trimming loop (trim) information."); } },
                        { "hole",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle inner trimming loop (hole) information."); } },
                        { "scrv",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle special curve (scrv) information."); } },
                        { "sp",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle special point (sp) information."); } },
                        { "end",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle end statement (end) information."); } },
                        { "con",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle connect (con) information."); } },
                        { "g",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle group name (g) information."); } },
                        { "s",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle smoothing group (s) information."); } },
                        { "mg",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle merging group (mg) information."); } },
                        { "o",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle object name (o) information."); } },
                        { "bevel",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle bevel interpolation (bevel) information."); } },
                        { "c_interp",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle color interpolation (c_interp) information."); } },
                        { "d_interp",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle dissolve interpolation (d_interp) information."); } },
                        { "lod",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle level of detail (lod) information."); } },
                        { "usemtl",
                            (line) => { linesMaterial.Add(line); } },
                        { "mtllib",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle material library (mtllib) information."); } },
                        { "shadow_obj",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle shadow casting (shadow_obj) information."); } },
                        { "trace_obj",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle ray tracing (trace_obj) information."); } },
                        { "ctech",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle curve approximation technique (ctech) information."); } },
                        { "stech",
                            (line) => { Debug.Log(SCRIPTNAME + "Cannot yet handle surface approximation technique (stech) information."); } },
                    };

                while (!f.EndOfStream) {
                    string line = f.ReadLine().Trim();

                    // Calls line handler using a cleaned up key
                    string keyword = line.Split(' ')[0].Trim();
                    if (null != keyword
                        && !string.Equals(keyword, "")
                        && !string.Equals(keyword, "#")) {
                        handlerLine[keyword](line);
                    }
                }
            }

            
            // TODO: break array creation into smaller pieces
            // Builds vertices array
            Vector3[] vertices = new Vector3[linesVertex.Count];
            for (int i = 0; i < linesVertex.Count; i++) {
                linesVertex[i] = linesVertex[i].Remove("v").Trim();
                string[] splitLineVertex = linesVertex[i].Split(' ');

                vertices[i] = new Vector3(
                    float.Parse(splitLineVertex[0], System.Globalization.CultureInfo.InvariantCulture),
                    float.Parse(splitLineVertex[1], System.Globalization.CultureInfo.InvariantCulture),
                    float.Parse(splitLineVertex[2], System.Globalization.CultureInfo.InvariantCulture)
                    );
            }

            // Builds vertex points array
            Vector3[] vertexPoints = new Vector3[linesVertexPoint.Count];

            // Builds vertex normals array
            Vector3[] vertexNormals = new Vector3[linesVertexNormal.Count];
            for (int i = 0; i < linesVertexNormal.Count; i++) {
                linesVertexNormal[i] = linesVertexNormal[i].Remove("vn").Trim();
                string[] splitLineVertexNormal = linesVertexNormal[i].Split(' ');

                vertexNormals[i] = new Vector3(
                    float.Parse(splitLineVertexNormal[0], System.Globalization.CultureInfo.InvariantCulture),
                    float.Parse(splitLineVertexNormal[1], System.Globalization.CultureInfo.InvariantCulture),
                    float.Parse(splitLineVertexNormal[2], System.Globalization.CultureInfo.InvariantCulture)
                    );
            }
            // Builds texture vertices array
            Vector2[] vertexTextures = new Vector2[linesVertexTexture.Count];
            for (int i = 0; i < linesVertexTexture.Count; i++) {
                linesVertexTexture[i] = linesVertexTexture[i].Remove("vt").Trim();
                string[] splitLineVertexTexture = linesVertexTexture[i].Split(' ');

                vertexTextures[i] = new Vector2(
                    float.Parse(splitLineVertexTexture[0], System.Globalization.CultureInfo.InvariantCulture),
                    float.Parse(splitLineVertexTexture[1], System.Globalization.CultureInfo.InvariantCulture)
                    );
            }

            // Ensures all vertex arrays are either 
            // empty or equal in size to number of vertices
            // NOTE: this is because unspecified vertex arrays are returned empty by Unity
            List<int> lengths = new List<int>() {
                (vertexPoints.Length != 0 ? vertexPoints.Length : vertices.Length),
                (vertexNormals.Length != 0 ? vertexNormals.Length : vertices.Length),
                (vertexTextures.Length != 0 ? vertexTextures.Length : vertices.Length) };
            Debug.Assert(lengths
                .Map((x) => x == vertices.Length)
                .FoldL(true, (b1, b2) => b1 == b2),
                SCRIPTNAME + "Mismatch in vertex array length.");


            // Builds triangles array
            List<int> trianglesTemp = new List<int>();
            for (int i = 0; i < linesTriangle.Count; i++) {
                linesTriangle[i] = linesTriangle[i].Remove("f").Trim();

                // Decreases indices by 1 since OBJ is 1-indexed
                List<int> splitLineTriangle = new List<int>();
                foreach (string trio in linesTriangle[i].Split(' ')) {
                    string s = trio.Split('/')[0];
                    splitLineTriangle.Add(int.Parse(s.Trim()));
                }
                splitLineTriangle = splitLineTriangle.Map((x) => x - 1);

                // Assigns triangles in threes
                for (int j = 0; j < splitLineTriangle.Count - 2; j++) {
                    trianglesTemp.Add(splitLineTriangle[0]);
                    trianglesTemp.Add(splitLineTriangle[j + 1]);
                    trianglesTemp.Add(splitLineTriangle[j + 2]);
                }             
            }

            int[] triangles = new int[trianglesTemp.Count];
            for (int i = 0; i < trianglesTemp.Count; i++) {
                triangles[i] = trianglesTemp[i];
            }

            // Ensures triangles array is properly constructed
            Debug.Assert(triangles.Length % 3 == 0,
                SCRIPTNAME + "Triangles array length not divisible by three.");

            // Builds materials array
            // TEST (hardcoded directory)
            List<Material> materials = new List<Material>();
            for (int i = 0; i < linesMaterial.Count; i++) {
                linesMaterial[i] = linesMaterial[i].Remove("usemtl").Trim() + ".mtl";
                materials.Add(ReadMaterial(linesMaterial[i], Path.Combine(Application.dataPath, DIRECTORY), rnd));
            }

            // Assigns arrays to output mesh
            output.vertices = vertices;
            output.uv = vertexTextures;
            output.normals = vertexNormals;
            output.triangles = triangles;

            // Assigns materials to renderer
            if (null != rnd) {
                for (int i = 0; i < materials.Count; i++) {
                    rnd.materials[i] = materials[i];
                }
            }

            output.name = "Mesh_" + Path.GetFileNameWithoutExtension(path);
            return output;
        }

        // TEST (directoryTexture and directoryImage are equal in tex.LoadImage(...))
        // TEST (setting rnd texture here)
        public static Material ReadMaterial(string filename, string directory, Renderer rnd = null)
        {
            // Checks if file in filename is a compatible MTL file
            // TODO: use a file analyser
            Debug.Assert(string.Equals(
                Path.GetExtension(filename).ToUpperInvariant(),
                ".mtl".ToUpperInvariant()));

            Material output = new Material(Shader.Find("Standard"));
            Texture2D tex = new Texture2D(256, 256);
            Debug.Assert(tex.LoadImage(FindTextureImage(filename, directory, directory)),
                SCRIPTNAME + "Failed to load texture image.");

            if (null != rnd) {
                rnd.material.mainTexture = tex;
            }

            return output;
        }

        #region Helpers
        // Returns a new string with all strings in substrings removed from original
        private static string Remove(this string original, List<string> substrings)
        {
            string output = original;
            foreach (string sub in substrings) {
                output = output.Replace(sub, "");
            }
            return output;
        }
        private static string Remove(this string original, string substring)
        {
            string output = original;
            output = output.Replace(substring, "");
            return output;
        }

        private static byte[] FindTextureImage(string filenameTexture, string directoryTexture, string directoryImage)
        {
            string path = Path.Combine(directoryTexture, filenameTexture);
            string filenameImage = null;
            using (StreamReader f = new StreamReader(path)) {
                while (!f.EndOfStream) {
                    string line = f.ReadLine().Trim();
                    string[] words = line.Split(' ');
                    string keyword = words[0];

                    if (keyword.Contains("map")) {
                        filenameImage = words[words.Length - 1];
                    } 
                }
                Debug.Assert(null != filenameImage,
                    SCRIPTNAME + "Failed to find texture image.");
            }

            // Checks if file in filename is a compatible JPG or PNG file
            // TODO: use a file analyser
            Debug.Assert(
                string.Equals(Path.GetExtension(filenameImage).ToUpperInvariant(),
                ".jpg".ToUpperInvariant())
                || string.Equals(Path.GetExtension(filenameImage).ToUpperInvariant(),
                ".jpeg".ToUpperInvariant())
                || string.Equals(Path.GetExtension(filenameImage).ToUpperInvariant(),
                ".png".ToUpperInvariant()),
                SCRIPTNAME + "Incompatible texture image format."); 

            return File.ReadAllBytes(Path.Combine(directoryImage, filenameImage));    
        }
        #endregion

    }
}