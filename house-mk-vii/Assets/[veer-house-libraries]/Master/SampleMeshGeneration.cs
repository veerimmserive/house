﻿/// File: SampleMeshGeneration.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolkit
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
	public class SampleMeshGeneration : MonoBehaviour
	{
		private const string SCRIPTNAME = "SampleMeshGeneration.cs: ";

        private int rows = 4;
        private int cols = 10;

        private Vector3[] vertices;
        #region MonoBehaviours
        void Start()
        {
            // Generates vertices
            vertices = new Vector3[(rows + 1) * (cols + 1)];
            for (int i = 0, row = 0; row < rows; row++) {
                for (int col = 0; col < cols; col++, i++) {
                    vertices[i] = new Vector3(col, row);
                }
            }

            // Sets up a MeshRenderer component on this object
            MeshRenderer rnd = this.gameObject.GetComponent<MeshRenderer>();
            MeshFilter flt = this.gameObject.GetComponent<MeshFilter>();
            Mesh mesh = MeshGeneration.GenerateMesh(vertices);
            flt.mesh = mesh;

            // Generates triangles
            int[] triangles = new int[rows * cols * 6];
            StartCoroutine(Generate(triangles, mesh));           
        }

		void Update () {
			
		}

        void OnDrawGizmos()
        {
            // Draws spheres at vertices
            if (vertices != null) {
                for (int i = 0; i < vertices.Length; i++) {
                    Gizmos.DrawSphere(vertices[i], 0.1f);
                }
            }
        }
		#endregion
		
        public IEnumerator Generate(int[] triangles, Mesh mesh)
        {
            for (int row = 0; row < rows - 1; row++) {
                for (int ti = row * cols * 6, vi = row * cols, col = 0; col < cols; ti += 6, vi++, col++) {
                    Debug.LogFormat("{0}: ti = {1}, vi = {2}, col = {3}, row = {4}", SCRIPTNAME, ti, vi, col, row);
                    triangles[ti + 0] = vi;
                    triangles[ti + 1] = vi + cols;
                    triangles[ti + 2] = vi + cols + 1;
                    triangles[ti + 3] = vi + cols + 1;
                    triangles[ti + 4] = vi + 1;
                    triangles[ti + 5] = vi;
                    mesh.triangles = triangles;
                    yield return new WaitForSeconds(0.1f);
                }
            }
        }
	}
}