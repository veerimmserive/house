﻿/// File: TestInputOutput.cs
/// Description: 

using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Veer.House.InputOutput;
using Veer.House.Modelling;

namespace Veer.House
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class TestInputOutput : MonoBehaviour
	{
		private const string SCRIPTNAME = "TestInputOutput.cs: ";

        #region MonoBehaviours
        Mesh mesh = new Mesh();
        List<Face> fs = new List<Face>();
        
        void Start () {
            MeshFilter flt = this.gameObject.GetComponent<MeshFilter>();
            MeshRenderer rnd = this.gameObject.GetComponent<MeshRenderer>();

            // Reads in mesh
            string directory1 = "C:/TestInputOutput";
            string directory2 = Path.Combine(
                Application.dataPath,
                "[toolkit-mesh-manipulation]/Experimental/Tests/TestInputOutput");
            string filename = "capsule.obj";
            string path = Path.Combine(directory2, filename);
            mesh = FileIO.Read(directory2, filename, rnd);
            Debug.Log(SCRIPTNAME + string.Format("Reading in mesh at '{0}'...", path));

            mesh.RecalculateNormals();

            // Tests triangulation
            System.Func<Mesh, List<Face>> Triangulate = (m) => {
                List<Face> output = new List<Face>();

                for (int i = 0; i + 2 < m.triangles.Length; i += 3) {
                    Face f = new Face(
                        new Point(m.vertices[m.triangles[i + 0]]),
                        new Point(m.vertices[m.triangles[i + 1]]),
                        new Point(m.vertices[m.triangles[i + 2]]));
                    output.Add(f);
                }

                return output;
            };

            fs = Triangulate(mesh);
            print(fs.Count);

            MeshFilter[] flts = new MeshFilter[fs.Count / 5];
            CombineInstance[] combine = new CombineInstance[flts.Length];
            for (int i = 0; i < fs.Count; i += 5) {
                flts[i].mesh = fs[i].GetMesh();
                combine[i].mesh = flts[i].mesh;
                combine[i].transform = flts[i].transform.localToWorldMatrix;
            }
            flt.mesh.CombineMeshes(combine);
            

        }

        void FixedUpdate() {
            
        }

		
		#endregion
		
	}
}