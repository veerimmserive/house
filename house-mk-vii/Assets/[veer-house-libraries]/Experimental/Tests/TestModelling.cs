﻿/// File: TestModelling.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Veer.House.Modelling;

namespace Veer.House
{
	public class TestModelling : MonoBehaviour
	{
		private const string SCRIPTNAME = "TestModelling.cs: ";

        #region MonoBehaviours
        List<Vector4> vs = new List<Vector4>();
        List<Point> ps = new List<Point>();
        List<Edge> es = new List<Edge>();
        List<Face> fs = new List<Face>();
        List<Face> fs_prime = new List<Face>();
        void Update () {
            #region Simplex construction
            for (int i = 0; i < 3; i++) {
                ps.Add(new Point(
                    Random.Range(-i, i),
                    2f * Random.Range(i, -i),
                    Random.Range(-i, i)));
            }
            Edge e1 = (ps.Count > 0 ? new Edge(ps[0], ps[1]) : new Edge(new Point(), new Point()));
            Edge e2 = (ps.Count > 0 ? new Edge(ps[1], ps[2]) : new Edge(new Point(), new Point()));
            Edge e3 = (ps.Count > 0 ? new Edge(ps[2], ps[0]) : new Edge(new Point(), new Point()));           
            es.Add(e1);
            es.Add(e2);
            es.Add(e3);
            foreach (Edge e in es) {
                Debug.Assert(2 == e.Points.Count);
                Debug.Assert(1 == e.Dimension);
                Debug.Assert(null != e.Start);
                Debug.Assert(null != e.End);
            }
            Face f = new Face(ps[0], ps[1], ps[2]);
            fs.Add(f);
            Debug.Assert(3 == f.Points.Count);
            Debug.Assert(2 == f.Dimension);
            #endregion

            #region GetMesh
            Mesh m = f.GetMesh();
            Debug.Assert(null != m.vertices);
            Debug.Assert(null != m.triangles);
            #endregion

            #region GetStar
            Complex c = new Complex(new List<Simplex> { f });
            Simplex s_face = f.Faces[4];
            List<Simplex> star = s_face.GetStar(c);        
            Debug.Assert(0 != star.Count);
            print("Star of " + s_face);
            foreach (Simplex s in star) {
                print(s);
            }
            print("DONE");
            #endregion

            #region GetLink
            Face f_link1 = new Face(new Point(), new Point(1, 0), new Point(0, 1));
            Face f_link2 = new Face(new Point(1, 1), f_link1.V3.Point, f_link1.V2.Point);
            fs_prime.Add(f_link1);
            fs_prime.Add(f_link2);
            Complex c_prime = new Complex(new List<Simplex> { f_link1, f_link2 });
            Simplex f_face = f_link1.GetSubfaces(1)[0];
            print("Link of " + f_face);
            foreach (Simplex s in f_face.GetLink(c_prime).Simplices) {
                print(s);
            }
            print("DONE");
            #endregion

            #region Equality methods
            Vertex v1 = new Vertex(new Point());
            Vertex v2 = new Vertex(v1.Point);
            Vertex v3 = new Vertex(new Point());
            Debug.Assert(v1.Equals(v2));
            Debug.Assert(!v1.Equals(v3));

            Face g1 = new Face(v1, v2, v3);
            Face g2 = new Face(v1, v2, v3);
            Debug.Assert(g1.Equals(g2));

            Complex c1 = new Complex(f.Faces);
            Complex c2 = new Complex(f.Faces);
            foreach (Simplex s in c1.Simplices) {
                bool HasEqualSimplex = false;
                foreach (Simplex t in c2.Simplices) {
                    if (s == t) {
                        HasEqualSimplex = true;
                        break;
                    }
                }
                Debug.Assert(HasEqualSimplex);
            }
            Debug.Assert(c1.Equals(c2));
            #endregion



            # region Complex.GetDimension()
            Debug.Assert(2 == c.GetDimension());
            Debug.Assert(1 == new Complex(new List<Simplex> { e1, e2, e3 }).GetDimension());
            #endregion

            #region Complex.GetSkeleton()
            foreach (var i in c_prime.GetSkeleton(2)) {
                print(i);
            }
            #endregion

        }

        void OnDrawGizmos()
        {
            #region Drawing from simplices
            foreach (Edge e in es) {
                Gizmos.color = Color.white;
                Gizmos.DrawLine(e.Start.Coordinates, e.End.Coordinates);
                Gizmos.color = Color.green;
                Gizmos.DrawSphere(e.Start.Coordinates, 0.05f);
                Gizmos.DrawSphere(e.End.Coordinates, 0.05f);
            }

            foreach (Simplex s in fs[0].Faces) {
                Gizmos.color = Color.red;
                Gizmos.DrawCube(s.Centroid.Coordinates, 0.01f * Vector3.one);
            }

            Mesh m = fs[0].GetMesh();
            Gizmos.color = Color.white;
            Gizmos.DrawMesh(m);
            #endregion

            #region Drawing links
            foreach (Face f in fs_prime) {
                Mesh m_prime = f.GetMesh();
                Gizmos.color = Color.white;
                Gizmos.DrawMesh(m_prime);
            }
            #endregion
        }
        #endregion



    }
}