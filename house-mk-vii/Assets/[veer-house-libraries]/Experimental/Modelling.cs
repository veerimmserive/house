﻿/// File: Modelling.cs
/// Description: 
///     Class definitions based on Stroud, I., Boundary Representation
///     Modelling Techniques (see Appendices)

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
namespace Veer.House.Modelling
{
    
	public static class Graphics
	{
        // TODO: fiddle around with this value
        public const float EPSILON = float.Epsilon;
        public struct Vertex
        {
            public float X;
            public float Y;
            public float Z;
            public float W;
        }

        public struct VertexNormal
        {
            public float X;
            public float Y;
            public float Z;
        }

        public struct VertexTexture
        {
            public float U;
            public float V;
            public float W;
        }

        // TEST
        public static Mesh GenerateMesh(Geometry.Vertex[] _vertices)
        {
            Mesh output = new Mesh();
            output.vertices = new Vector3[_vertices.Length];
            for (int i = 0; i < _vertices.Length; i++) {
                output.vertices[i] = new Vector3(
                    _vertices[i].Coordinates.x,
                    _vertices[i].Coordinates.y,
                    _vertices[i].Coordinates.z);
            }

            return output;
        }

    }

    public static class Geometry
    {
        // TODO: fiddle around with this value
        public const float EPSILON = float.Epsilon;

        #region Primitives
        public class Vertex
        {
            public float X { get; private set; }
            public float Y { get; private set; }
            public float Z { get; private set; }
            public float W { get; private set; }
            // TODO: test for (0, 0, 0, 0), which should never be a point
            public Vector4 Coordinates { get; private set; }

            public Vertex(Graphics.Vertex _Vertex)
            {
                this.X = _Vertex.X;
                this.Y = _Vertex.Y;
                this.Z = _Vertex.Z;
                this.W = _Vertex.W;
                this.Coordinates = new Vector4(
                    _Vertex.X,
                    _Vertex.Y,
                    _Vertex.Z,
                    _Vertex.W);
            }

            public Vertex()
            {
                this.X = 0f;
                this.Y = 0f;
                this.Z = 0f;
                this.W = 1f;
                this.Coordinates = new Vector4(
                    0f,
                    0f,
                    0f,
                    1f);
            }

            public Vertex(float _X)
            {
                this.X = X;
                this.Y = 0f;
                this.Z = 0f;
                this.W = 1f;
                this.Coordinates = new Vector4(
                    _X,
                    0f,
                    0f,
                    1f);
            }

            public Vertex(float _X, float _Y)
            {
                this.X = _X;
                this.Y = _Y;
                this.Z = 0f;
                this.W = 1f;
                this.Coordinates = new Vector4(
                    _X,
                    _Y,
                    0f,
                    1f);
            }

            public Vertex(float _X, float _Y, float _Z)
            {
                this.X = _X;
                this.Y = _Y;
                this.Z = _Z;
                this.W = 1f;
                this.Coordinates = new Vector4(
                    _X,
                    _Y,
                    _Z,
                    1f);
            }

            public Vertex(float _X, float _Y, float _Z, float _W)
            {
                this.X = _X;
                this.Y = _Y;
                this.Z = _Z;
                this.W = _W;
                this.Coordinates = new Vector4(
                    _X,
                    _Y,
                    _Z,
                    _W);
            }

            public Vertex(Vector3 coords)
            {
                this.X = coords.x;
                this.Y = coords.y;
                this.Z = coords.z;
                this.W = 1f;
                this.Coordinates = new Vector4(
                    coords.x,
                    coords.y,
                    coords.z,
                    1f);
            }

            public Vertex(Vector3 coords, float _W)
            {
                this.X = coords.x;
                this.Y = coords.y;
                this.Z = coords.z;
                this.W = _W;
                this.Coordinates = new Vector4(
                    coords.x,
                    coords.y,
                    coords.z,
                    _W);
            }
        }

        

        public class Edge
        {
            public Vertex Start { get; private set; }
            public Vertex End { get; private set; }
            public Vertex Midpoint { get; private set; }
            public Vertex[] Vertices { get; private set; }

            // TODO: test for inequality of _Start and _End
            public Edge(Vertex _Start, Vertex _End)
            {
                Start = new Vertex(_Start.Coordinates);
                End = new Vertex(_End.Coordinates);
                Midpoint = new Vertex(
                    0.5f * new Vector3(
                        _End.X - _Start.X,
                        _End.Y - _Start.Y,
                        _End.Z - _Start.Z));
                Vertices = new Vertex[] { _Start, _End };
            }
        }

        public class Face
        {
            public Vertex Center { get; private set; }
            public Vector3 Normal { get; private set; }
            public Edge[] Edges { get; private set; }
            public Vertex[] Vertices { get; private set; }  
            public Mesh Mesh { get; private set; }   
                                  
            // TODO: test for orientability
            // TODO: test for empty _Vertices
            public Face(Vertex[] _Vertices)
            {
                // Calculates center
                float xSum = 0;
                float ySum = 0;
                float zSum = 0;
                for (int i = 0; i < _Vertices.Length; i++) {
                    xSum += _Vertices[i].X;
                    ySum += _Vertices[i].Y;
                    zSum += _Vertices[i].Z;
                }
                float xAve = xSum / _Vertices.Length;
                float yAve = ySum / _Vertices.Length;
                float zAve = zSum / _Vertices.Length;
                Center = new Vertex(xAve, yAve, zAve);

                // TODO: calculates Normal

                // TODO: sets Edges

                // Sets vertices
                Vertices = _Vertices;

                // Generates Mesh
                Mesh = new Mesh();
                Mesh.Clear();
                Mesh.vertices = _Vertices.ToArray();
                int[] triangles = new int[3 * _Vertices.Length];
                for (int vi = 0, ti = 0; vi + 2 < _Vertices.Length; vi += 3) {
                    triangles[vi + 0] = 0;
                    triangles[vi + 1] = ti + 1;
                    triangles[vi + 2] = ti + 2;
                    ti++;
                }
                Mesh.triangles = triangles;      
            }          

            public Face(Edge[] _Edges)
            {

            }
        }

        #endregion

        #region Extensions
        // Gives you the Vector3[] representation of a Vertex[]
        public static Vector3[] ToArray(this Vertex[] vs)
        {
            Vector3[] output = new Vector3[vs.Length];
            for (int i = 0; i < vs.Length; i++) {
                output[i] = vs[i].Coordinates;
            }

            return output;
        }

       
        #endregion

        #region Transformations
        #endregion
    }

    public static class Shapes
    {
        public static GameObject CreateQ(float radius)
        {
            GameObject output = new GameObject("Sphere");

            Geometry.Face asdf = new Geometry.

            return output;
        }
    }
    
}
*/

/*
namespace Veer.House.Modelling
{
    #region Data Elements
    /// <summary>
    /// This namespace defines simplices as classes. Usually, CAGD works
    /// with points, but there is benefit to ascending the abstraction ladder
    /// and using simplices instead. This approach is based on the work of 
    /// Luo and Lukacs (1991).
    /// 
    /// To be considered valid, the elements below must satisfy the ff.
    /// 1) Each Face must be incident to a finite number of Loops.
    /// 2) Each Edge must be incident to a finite number of Wedges.
    /// 3) Each Vertex must be incident to a finite number of Bundles.
    /// 4) V - E + F = 2(S - H) + (B - W + L)
    ///     where   V := number of vertices
    ///             E := number of edges
    ///             F := number of faces
    ///             S := number of shells
    ///             H := number of holes on solid boundaries
    ///             B := number of extra bundles (a nondegenerate point has only one)
    ///             W := number of extra wedges (a nondegenerate edge has only one)
    ///             L := number of extra loops (a nondegenerate face has only one: its bounding loop)
    /// </summary>
    /// 

    // A 0D simplex defined at some Point
    public class Vertex
    {
        public Bundle Bundle { get; private set; }
        public Point Position { get; private set; }
        public Vertex Next { get; private set; }
        public Vertex Friend { get; private set; }
        // TODO: Cogeometry
        // TODO: Info
        // TODO: Group
        public ushort Marker { get; private set; }
        public int Number { get; private set; }
        // TODO: Name
    }

    // A set of simplices incident to a Vertex
    public class Bundle
    {
        public Edge Edge { get; private set; }
        public Loop Loop { get; private set; }
        public Vertex Vertex { get; private set; }
        public Bundle Next { get; private set; }
    }

    // A 1D simplex bounded by Vertex elements
    public class Edge
    {
        public Wedge Wedge { get; private set; }
        public Vertex Start { get; private set; }
        public Vertex End { get; private set; }
        public Vertex Next { get; private set; }
        public Vertex Previous { get; private set; }
        // TODO: Owner
        // TODO: Curve
        // TODO: Friend
        // TODO: Cogeometry
        // TODO: Info
        // TODO: Group
        // TODO: Space
        public ushort Marker { get; private set; }
        public int Number { get; private set; }
        // TODO: Name
    }

    // A set of simplices incident to an Edge
    public class Wedge
    {
        public Edge Edge { get; private set; }
        public LoopEdgeLink RightLink { get; private set; }
        public LoopEdgeLink LeftLink { get; private set; }
        public Wedge Next { get; private set; }
    }

    // A 2D simplex bounded by Edge elements arranged in a Loop
    public class Face
    {
        public Loop Loop { get; private set; }
        public Surface Surface { get; private set; }
        // TODO: FaceGroup
        public Face Next { get; private set; }
        // TODO: Feature
        public Face Friend { get; private set; }
        // TODO: Cogeometry
        public bool IsOutward { get; private set; }
        // TODO: Info
        // TODO: Group
        // TODO: Space
        public ushort Marker { get; private set; }
        // TODO: Name
        // TODO: Access
    }

    // A set of simplices incident to a Face
    public class Loop
    {
        public Face Face { get; private set; }
        // TODO: Link
        public Vertex Vertex { get; private set; }
        public Loop Next { get; private set; }
        // TODO: Info
        // TODO: Group
        public ushort Marker { get; private set; }
        public bool IsHoleloop { get; private set; }
        public int Number { get; private set; }
    }

    // Joins loops to edges; defines edge adjacency
    public class LoopEdgeLink
    {
        public Edge Edge { get; private set; }
        public Loop Loop { get; private set; }
        public LoopEdgeLink Next { get; private set; }
        public LoopEdgeLink Previous { get; private set; }
        public LoopEdgeLink Friend { get; private set; }
    }


    // A general collection of faces
    // Note: must not be exposed to the user
    public class FaceGroup
    {
        public Face Face { get; private set; }
        public FaceGroup FirstFaceGroup { get; private set; }
        public FaceGroup Next { get; private set; }
        // TODO: Owner
        public Surface Surface { get; private set; }
        // TODO: Cogeometry
        // TODO: Info
        // TODO: Group
        // TODO: Space
        public ushort Marker { get; private set; }
        public int Number { get; private set; }
        // TODO: Name
    }

    // A closed set of Face elements (analogous to the Loop)
    public class Shell
    {
        // TODO: Owner
        public Shell Next { get; private set; }
        public FaceGroup FaceGroup { get; private set; }
        // TODO: Info
        // TODO: Group
        // TODO: Space
        public ushort Marker { get; private set; }
        public int Number { get; private set; }
        // TODO: Name
    }


    #endregion

    #region Geometry
    public class Point
    {
        public Vector4 Position { get; private set; }
        // TODO: User
        // TODO: Info
        // TODO: Group
        public ushort Marker { get; private set; }
        public int CountUsed { get; private set; }
        public int Number { get; private set; }
    }

    public class Surface
    {

    }
    #endregion

    #region 
    #endregion
}
*/

namespace Veer.House.Modelling
{
    public class Point
    {
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }
        public float W { get; private set; }
        public Vector4 Coordinates { get; private set; }
        public float Magnitude { get; private set; }

        public Point() : this(0f, 0f, 0f, 1f)
        {
        }
        public Point(float _X) : this(_X, 0f, 0f, 1f)
        {
        }
        public Point(float _X, float _Y) : this(_X, _Y, 0f, 1f)
        {
        }
        public Point(float _X, float _Y, float _Z) : this(_X, _Y, _Z, 1f)
        {
        }
        public Point(Vector4 v) : this(v.x, v.y, v.z, v.w)
        {
        }
        public Point(float _X, float _Y, float _Z, float _W)
        {
            this.X = _X;
            this.Y = _Y;
            this.Z = _Z;
            this.W = _W;
            this.Coordinates = new Vector4(
                _X,
                _Y,
                _Z,
                _W);
            this.Magnitude = Coordinates.magnitude;
        }

        public Point(Vector3 coords) : this(coords, 1f)
        {
        }
        public Point(Vector3 coords, float _W)
        {
            this.X = coords.x;
            this.Y = coords.y;
            this.Z = coords.z;
            this.W = _W;
            this.Coordinates = new Vector4(
                coords.x,
                coords.y,
                coords.z,
                _W);
            this.Magnitude = Coordinates.magnitude;
        }   

        public static Point operator + (Point p1, Point p2)
        {
            return new Point(
                p1.X + p2.X,
                p1.Y + p2.Y,
                p1.Z + p2.Z,
                p1.W + p2.W);
        }

        public static Point operator - (Point p1, Point p2)
        {
            return new Point(
                p2.X - p1.X,
                p2.Y - p1.Y,
                p2.Z - p1.Z,
                p2.W - p1.W);
        }

        public static Point operator * (float f, Point p)
        {
            return new Point(f * new Vector4(p.X, p.Y, p.Z, p.W));
        }
        public static Point operator * (Point p, float f)
        {
            return new Point(f * new Vector4(p.X, p.Y, p.Z, p.W));
        }

    }
  
    public class Simplex : System.IEquatable<Simplex>
    {
        // TODO: convert these to readonly fields after C# 6 upgrade
        // TODO: convert these to Lazy<T> after C# 6 upgrade
        /// <summary>
        /// Contains only proper faces
        /// </summary>
        public List<Simplex> Faces { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public List<Point> Points { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public Point Centroid { get; private set; }
        /// <summary>
        /// Is defined as 
        /// </summary>
        public int Dimension { get; private set; }

        #region Constructors
        public Simplex(Point p) : this(new List<Point> { p })
        {
        }
        public Simplex(List<Point> ps)
        {
            // Checks for absurd dimensions
            if (ps.Count > 4) {
                throw new System.Exception("Are you sure you want a 4D object?");
            }

            this.Points = GetConvexHull(ps);
            this.Faces = GetFaces(Points);
            this.Centroid = GetCentroid(ps);
            this.Dimension = Points.Count - 1;
        }
        #endregion

        #region Methods
        // TODO: avoid redundant Simplex to Object to Simplex conversion
        // TODO: overload (==)
        /// <summary>
        /// Compares simplices by referentially comparing their Points.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool Equals(Simplex s)
        {
            return this.Equals((object) s);
        }
        public override bool Equals(object obj)
        {
            bool output = false;
            Simplex s = obj as Simplex;

            // Boilerplate checks
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != this.GetType())
                return false;

            // Checks base cases
            if (this.Points.Count != s.Points.Count) {
                return false;
            }

            // Compares simplices point by point
            output = true;
            for (int i = 0; i < this.Points.Count; i++) {
                bool HasEqualPoint = false;
                for (int j = 0; j < s.Points.Count; j++) {
                    if (ReferenceEquals(this.Points[i], s.Points[j])) {
                        HasEqualPoint = true;
                        break;
                    }
                }

                if (false == HasEqualPoint) {
                    return false;
                }
            }

            return output;
        }

        public override int GetHashCode()
        {
            unchecked {
                // Choose large primes to avoid hashing collisions
                const int HashingBase = (int) 2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                for (int i = 0; i < this.Points.Count; i++) {
                    hash = (hash * HashingMultiplier) ^ 
                        (!ReferenceEquals(null, this.Points[i]) ? Points[i].GetHashCode() : 0);
                }
                return hash;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="complex"></param>
        /// <returns></returns>
        public List<Simplex> GetStar(Complex complex)
        {
            return (from s in complex.Simplices
                   where s.Faces.Contains(this)
                   select s).Distinct().ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="complex"></param>
        /// <returns></returns>
        // TODO: fix this
        public Complex GetLink(Complex complex)
        {
            List<Simplex> star = this.GetStar(complex);
            List<Simplex> closure = new Complex(star).Simplices;
            List<Simplex> link_simplices = closure.Except(star.Union(this.Faces)).Distinct().ToList();
            link_simplices.Remove(this);

            return new Complex(link_simplices);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public List<Simplex> GetSubfaces(int dimension)
        {
            var output =
                from f in this.Faces
                where dimension == f.Dimension
                select f;

            return output.ToList<Simplex>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Mesh GetMesh()
        {
            Mesh output = new Mesh();

            output.vertices = this.Points.Select((x => (Vector3)x.Coordinates)).ToArray();
            
            int[] triangles = new int[3 * output.vertices.Length];
            for (int vi = 0, ti = 0; vi + 2 < output.vertices.Length; vi += 3) {
                triangles[vi + 0] = 0;
                triangles[vi + 1] = ti + 1;
                triangles[vi + 2] = ti + 2;
                ti++;
            }
            output.triangles = triangles;
            output.RecalculateNormals();

            return output;
        }
        #endregion

        #region Helpers     
        // TODO: implement a proper n-dimensional convex hull algorithm
        private List<Point> GetConvexHull(List<Point> ps)
        {
            return ps;
        }

        private List<Simplex> GetFaces(List<Point> ps)
        {
            List<Simplex> output = new List<Simplex>();

            // Checks for base cases
            if (1 >= ps.Count) {
                return output;
            } else {
                System.Func<List<Point>, List<List<Point>>> GetProperSubsets =
                    (_ps) => {
                        List<List<Point>> temp = new List<List<Point>>();

                        // Exploits a mapping between the binary representation
                        // of 2^n and the elements of the power set of a set.
                        // Essentially, "1" means include in the subset and
                        // "0" to exclude.
                        for (int i = 0; i < (int) Mathf.Pow(2, _ps.Count) - 1; i++) {
                            List<Point> l = new List<Point>();
                            int i_prime = i;
                            int count = 0;
                            while (i_prime != 0) {
                                if (1 == (1 & i_prime)) {
                                    l.Add(_ps[count]);
                                }

                                i_prime >>= 1;
                                count++;
                            }
                            temp.Add(l);
                        }
                        return temp;
                    };

                // Assigns simplices to subsets
                List<List<Point>> subsets = GetProperSubsets(ps);
                for (int i = 0; i < subsets.Count; i++) {
                    int dim = subsets[i].Count - 1;
                    switch (dim) {
                        case -1:
                            break;
                        case 0:
                            output.Add(new Vertex(subsets[i][0]));
                            break;
                        case 1:
                            output.Add(new Edge(subsets[i][0], subsets[i][1]));
                            break;
                        case 2:
                            output.Add(new Face(subsets[i][0], subsets[i][1], subsets[i][2]));
                            break;
                        default:
                            output.Add(new Simplex(subsets[i]));
                            break;                   
                    }
                }
            }

            return output;
        }

        private Point GetCentroid(List<Point> ps)
        {
            Point output = new Point();

            // Checks for base cases
            if (0 == ps.Count) {
                return output;
            }

            for (int i = 0; i < ps.Count; i++) {
                output += ps[i];
            }
            output *= 1f / ps.Count;

            return output;
        }
        #endregion        
    }

    public class Vertex : Simplex
    {
        public Point Point { get; private set; }
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }
        public float W { get; private set; }
        public Vector4 Coordinates { get; private set; }
        public float Magnitude { get; private set; }
        public List<Bundle> Bundles; // TODO

        #region Constructors
        public Vertex(Point _p) : base(_p)
        {
            this.Point = _p;
            this.X = _p.X;
            this.Y = _p.Y;
            this.Z = _p.Z;
            this.W = _p.W;
            this.Coordinates = _p.Coordinates;
            this.Magnitude = _p.Magnitude;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return string.Format("Vertex{0}", this.Point.Coordinates);
        }
        #endregion
    }

    public class Edge : Simplex
    {
        public Vertex Start { get; private set; }
        public Vertex End { get; private set; }
        public Vector4 Direction { get; private set; }
        public List<Wedge> Wedges; // TODO

        #region Constructors
        public Edge(Vertex _start, Vertex _end) : this(
            new Point(_start.Coordinates),
            new Point(_end.Coordinates))
        {
        }
        public Edge(Point _start, Point _end) : base(new List<Point> { _start, _end })
        {
            this.Direction = _end.Coordinates - _start.Coordinates;
            this.Start = new Vertex(_start);
            this.End = new Vertex(_end);
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return string.Format("Edge: {0} -> {1}", this.Start.Coordinates, this.End.Coordinates);
        }
        #endregion
    }

    public class Face : Simplex
    {
        public Vertex V1 { get; private set; }
        public Vertex V2 { get; private set; }
        public Vertex V3 { get; private set; }
        public Vector4 Normal { get; private set; }
        public List<Loop> Loops; // TODO

        #region Constructors
        public Face(Vertex v1, Vertex v2, Vertex v3) : this(v1.Point, v2.Point, v3.Point)
        {
        }
        public Face(Point p1, Point p2, Point p3) : base(new List<Point> { p1, p2, p3 })
        {
            V1 = new Vertex(p1);
            V2 = new Vertex(p2);
            V3 = new Vertex(p3);
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return string.Format("Face: {0} -> {1} -> {2}",
                this.V1.Coordinates,
                this.V2.Coordinates,
                this.V3.Coordinates);
        }
        #endregion

    }

    public class Complex : System.IEquatable<Complex>
    {
        public List<Simplex> Simplices { get; private set; }

        #region Constructors
        public Complex(List<Simplex> ss)
        {
            // Gets all proper faces of simplices first, then adds the simplices
            List<Simplex> validFaces = 
                (from s in ss
                 from f in s.Faces
                 select f).Distinct().ToList();
            validFaces.AddRange(from s in ss select s);
            validFaces = validFaces.Distinct().ToList();

            // TODO: verify this
            List<Simplex> validIntersections =
                (from s in validFaces
                 from t in validFaces
                 where !s.Faces.Intersect(t.Faces).Any() ||
                       (!s.Faces.Except(s.Faces.Intersect(t.Faces)).Any() &&
                       !t.Faces.Except(s.Faces.Intersect(t.Faces)).Any())
                 select s).Distinct().ToList();

            this.Simplices = validIntersections;



        }
        #endregion

        #region Methods
        public bool Equals(Complex c)
        {
            return this.Simplices.SequenceEqual(c.Simplices);
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            unchecked {
                // Choose large primes to avoid hashing collisions
                const int HashingBase = (int) 2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                for (int i = 0; i < this.Simplices.Count; i++) {
                    hash = (hash * HashingMultiplier) ^
                        (!ReferenceEquals(null, this.Simplices[i]) ? Simplices[i].GetHashCode() : 0);
                }
                return hash;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Point> GetPoints()
        {
            return (from s in this.Simplices
                    from p in s.Points
                    select p).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetDimension()
        {
            return this.Simplices
                .OrderByDescending(x => x.Dimension)
                .ToList()[0]
                .Dimension;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public List<Simplex> GetSkeleton(int dimension)
        {
            return (from s in this.Simplices
                   where s.Dimension <= dimension
                   select s).ToList();
        }


        #endregion
    }

    public class Bundle
    {

    }

    public class Wedge
    {

    }

    public class Loop
    {

    }
}