﻿/// File: Operations.cs
/// Description: 

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.House.Operations
{
    public static class Operations
    {

        private const string SCRIPTNAME = "Operations.cs: ";

        public static List<U> Map<T, U>(this List<T> list, Func<T, U> f)
        {
            List<U> output = new List<U>();
            foreach (T element in list) {
                output.Add(f(element));
            }

            return output;
        }

        public static List<T> Filter<T>(this List<T> list, Func<T, bool> p)
        {
            List<T> output = new List<T>();
            foreach (T element in list) {
                if (p(element)) { list.Add(element); }
            }

            return output;
        }

        public static U FoldL<T, U>(this List<T> list, U acc, Func<U, T, U> f)
        {
            U output = acc;
            for (int i = 0; i < list.Count; i++) {
                output = f(output, list[i]);
            }

            return output;
        }

        public static U FoldR<T, U>(this List<T> list, Func<T, U, U> f, U acc)
        {
            U output = acc;
            for (int i = list.Count; 0 < i; i--) {
                output = f(list[i], output);
            }

            return output;
        }

    }
}