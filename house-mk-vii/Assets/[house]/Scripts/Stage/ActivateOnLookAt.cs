﻿/// File: FSMActivateOnLookAt.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.House
{
    public class ActivateOnLookAt : MonoBehaviour, IStateMachineFinite<bool>
    {
        private const string SCRIPTNAME = "ActivateOnLookAt.cs: ";

        #region MonoBehaviours
        [SerializeField]
        private Looker looker;
        void OnEnable()
        {
            looker.DoLookAt += OnLookAt;
        }

        void OnDisable()
        {
            looker.DoLookAt -= OnLookAt;
        }

        void OnLookAt(ActivateOnLookAt target)
        {
            if (this.Equals(target)) {
                DoRespond(true);
            }
        }
        #endregion

        #region IStateMachineFinite
        //public IStateFinite<bool> stateCurrent { get; private set; }
        public void DoRespond(bool signal)
        {
            //stateCurrent.Respond(signal);
        }

        #endregion

    }
}