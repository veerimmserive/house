﻿/// File: Grid.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.House
{
    [RequireComponent(typeof(GenerateGrid))]
	public class EntityGrid : MonoBehaviour
	{
		private const string SCRIPTNAME = "EntityGrid.cs: ";

        #region MonoBehaviours

        private GenerateGrid generateGrid;
		void Start () {
            generateGrid = this.GetComponent<GenerateGrid>();
            generateGrid.GenerateGridRectangular();
		}
		
		void Update () {
		}
		#endregion
		
	}
}