﻿/// File: GenerateGrid.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.House
{
	public class GenerateGrid : MonoBehaviour
	{

        [SerializeField]
        private EntityGridCell cell;

        [SerializeField]
        private int rows, cols = 0;

		public void GenerateGridRectangular(float spacing = 1f)
        {
            for (int row = 0; row < rows; row++) {
                for (int col = 0; col < cols; col++) {
                    string name = string.Format("Cell{0:000}x{1:000}", row + 1, col + 1);
                    GameObject goCell = GameObject.Instantiate(cell.gameObject);
                    goCell.name = name;
                    goCell.transform.parent = this.transform;
                    goCell.transform.localPosition = spacing * new Vector3(row, 0f, col);
                    
                }
            }
        }
		
	}
}