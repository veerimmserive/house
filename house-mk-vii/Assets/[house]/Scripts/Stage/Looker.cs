﻿/// File: Looker.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.House
{
	public class Looker : MonoBehaviour
	{
		private const string SCRIPTNAME = "Looker.cs: ";

		#region MonoBehaviours
		void Start () {
			
		}

        [SerializeField]
        private float range = 8f;
        public delegate void OnLookAt(ActivateOnLookAt target);
        public event OnLookAt DoLookAt;
        ActivateOnLookAt target = new ActivateOnLookAt();

        void FixedUpdate () {
            // Calls event DoLookAt
            Vector3 toForward = this.transform.TransformDirection(Vector3.forward);
            RaycastHit hitInfo = new RaycastHit();
            if (Physics.Raycast(this.transform.position, toForward, out hitInfo, range)) {
                target = hitInfo.transform.gameObject.GetComponent<ActivateOnLookAt>();
                if (null != target) {
                    DoLookAt(target);
                }

                print(SCRIPTNAME + hitInfo.transform.gameObject.name);
            }
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(this.transform.position, this.transform.position + this.transform.forward);
        }

        
		#endregion
		
	}
}