﻿/// File: ICommand.cs
/// Description: a callable, storable bijective function (which may have
///     side effects)

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.House
{
    public interface ICommand<T>
    {
        /// <summary>
        /// A (deep) copy of the input
        /// </summary>
        T inputLast { get; set; }

        /// <summary>
        /// What the command does; must set inputLast
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        T Apply(T input);

        /// <summary>
        /// A method that undoes Apply(); must set inputLast
        /// </summary>
        /// <param name="output"></param>
        /// <returns></returns>
        T Unapply(T input);
	}
}