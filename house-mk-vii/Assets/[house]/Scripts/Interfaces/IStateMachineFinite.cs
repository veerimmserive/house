﻿/// File: IStateMachineFinite.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.House
{
	public interface IStateMachineFinite<TSignal>
	{
        /// <summary>
        /// 
        /// </summary>
        //IStateFinite<TSignal> stateCurrent { get; }

        /// <summary>
        /// Gives signal to stateCurrent and assigns an
        /// IStateFinite to stateCurrent
        /// </summary>
        /// <param name="signal"></param>
        void DoRespond(TSignal signal);
    }
}