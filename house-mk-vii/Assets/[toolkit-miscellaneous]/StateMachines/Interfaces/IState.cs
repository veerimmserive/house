﻿/// File: IStateFinite.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolkit
{
	public interface IState
	{
        /// <summary>
        /// A method that always gets executed upon entering this state;
        /// note that this must NOT depend on the previous state
        /// </summary>
        void Enter();

        /// <summary>
        /// Returns an IState based on TSignal
        /// </summary>
        /// <param name="signal"></param>
        /// <returns></returns>
        IState Respond<TSignal>(TSignal input);

        /// <summary>
        /// A method that always gets executed before leaving this state;
        /// note that this must NOT depend on the previous state
        /// </summary>
        void Exit();
        		
	}
}