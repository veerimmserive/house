﻿/// File: AStateSystem.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolkit
{
    public abstract class StateSystem
    {
        public List<StateMachine> StateMachines { get; private set; }
    }
}