﻿/// File: StateMachine.cs
/// Description: 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Toolkit
{
	public abstract class StateMachine : MonoBehaviour
	{
		private const string SCRIPTNAME = "StateMachine.cs: ";

        public abstract TSignal Validate<TSignal>(object input);

        public IState StateCurrent { get; private set; }
        void Respond<TSignal>(TSignal input)
        {
            IState stateNew = StateCurrent.Respond(Validate<TSignal>(input));

            // Handles Enter() and Exit()
            // TODO: verify when equality holds
            if (StateCurrent != stateNew) {
                StateCurrent.Exit();
                StateCurrent = stateNew;
                StateCurrent.Enter();
            }
        }
		
	}
}