﻿// File: ErrorChecking.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.Toolkit
{
	public static class ErrorChecking { 

		private const string SCRIPTNAME = "ErrorChecking.cs: ";

		public static bool IsNull<T>(T t)
        {
            if (null == t) {
                Debug.LogError(SCRIPTNAME + "tried to access nonexistent object.");
                return true;
            } else {
                return false;
            }
        }

	}
}