﻿// File: Operations.cs
// Description: a library of useful higher-order functions

using System;
using System.Collections;
using System.Collections.Generic;

namespace Veer.Toolkit
{
	public static class Operations
	{

		private const string SCRIPTNAME = "Operations.cs: ";

		public static List<U> Map<T, U>(List<T> list, Func<T, U> f)
        {
            List<U> output = new List<U>();
            foreach(T element in list) {
                output.Add(f(element));
            }

            return output;
        }

        public static List<T> Filter<T>(List<T> list, Func<T, bool> p)
        {
            List<T> output = new List<T>();
            foreach (T element in list) {
                if (p(element)) { list.Add(element); }
            }

            return output;
        }

        public static U FoldL<T, U>(List<T> list, U acc, Func<U, T, U> f)
        {
            for (int i = 0; i < list.Count; i++) {
                acc = f(acc, list[i]);
            }

            return acc;
        }

        public static U FoldR<T, U>(List<T> list, Func<T, U, U> f, U acc)
        {
            for (int i = list.Count; 0 < i; i--) {
                acc = f(list[i], acc);
            }

            return acc;
        }

    }
}