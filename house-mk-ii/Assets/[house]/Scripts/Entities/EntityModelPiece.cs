﻿// File: EntityModelPiece.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Veer.House
{
	public class EntityModelPiece : MonoBehaviour
	{
		private const string SCRIPTNAME = "EntityModelPiece.cs: ";

        [HideInInspector]
        public Transform targetSyncPosition;

        private ManagerScaleModel manager;

        void Start()
        {
            manager = FindObjectOfType<ManagerScaleModel>();
        }
	}
}