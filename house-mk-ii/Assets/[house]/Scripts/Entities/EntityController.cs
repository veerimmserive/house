﻿// File: EntityController.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace Veer.House
{
    [RequireComponent(typeof(SteamVR_TrackedObject))]
    [RequireComponent(typeof(VRTK_ControllerEvents))]
    [RequireComponent(typeof(VRTK_ControllerActions))]
    public class EntityController : MonoBehaviour
	{

		private const string SCRIPTNAME = "EntityController.cs: ";

        private SteamVR_TrackedObject trackedObject;
        private VRTK_ControllerEvents events;
        private VRTK_ControllerActions actions;


        void Awake()
        {
            trackedObject = this.GetComponent<SteamVR_TrackedObject>();
            events = this.GetComponent<VRTK_ControllerEvents>();
            actions = this.GetComponent<VRTK_ControllerActions>();

            // Sets up controller events
            events.ApplicationMenuPressed += new ControllerInteractionEventHandler(DoApplicationMenuPressed);
        }

        void DoApplicationMenuPressed(object sender, ControllerInteractionEventArgs e)
        {
        }


	}
}