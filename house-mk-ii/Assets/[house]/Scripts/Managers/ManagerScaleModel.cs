﻿// File: ManagerScaleModel.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Veer.Toolkit;

namespace Veer.House
{
	public class ManagerScaleModel : MonoBehaviour
	{

		private const string SCRIPTNAME = "ManagerScaleModel.cs: ";

        #region Singleton
        private static ManagerScaleModel instance;
        private void Awake()
        {
            if (ErrorChecking.IsNull<ManagerScaleModel>(instance)) {
                instance = this;
            } else {
                DestroyImmediate(this.gameObject);
            }
        }
        #endregion

        #region State
        public enum State
        {
            Inactive,
            Active
        };

        [HideInInspector]
        public State state;
        #endregion

        // Sets scale of scale model
        [SerializeField]
        public float factorScale = 0.25f;

        [SerializeField]
        private Vector3 posOrigin = Vector3.zero;

        private ManagerStage stage;
        private List<GameObject> modelPieces = new List<GameObject>();

        public void ToggleModel()
        {
            switch (state) {
                case State.Active:
                    state = State.Inactive;
                    DestroyModel();
                    break;
                case State.Inactive:
                    state = State.Active;
                    CreateModel();
                    break;
            }
        }

        // Generalise GameObject
        private void CreateModel()
        {
            foreach (Transform child in stage.transform) {
                GameObject go = (GameObject) Instantiate(
                    child.gameObject,
                    factorScale * child.position + posOrigin,
                    child.rotation,
                    this.transform);
                go.transform.localScale = factorScale * child.transform.localScale;

                // Ensures go is synced with target
                go.AddComponent<EntityModelPiece>().targetSyncPosition = child;


                modelPieces.Add(go);
            }           
        }

        private void DestroyModel()
        {
            foreach (GameObject go in modelPieces) {
                DestroyImmediate(go);
            }
        }

        private void Start()
        {
            stage = FindObjectOfType<ManagerStage>();

        }
        

	}
}