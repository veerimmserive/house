﻿// File: ManagerStage.cs
// Description: 
// How to use:
// 	0) 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Veer.Toolkit;

namespace Veer.House
{
	public class ManagerStage : MonoBehaviour
	{

		private const string SCRIPTNAME = "ManagerStage.cs: ";

        #region Singleton
        private static ManagerStage instance;
        private void Awake()
        {
            if (ErrorChecking.IsNull<ManagerStage>(instance)) {
                instance = this;
            } else {
                DestroyImmediate(this.gameObject);
            }
        }        
        #endregion

        void Start () {
			
		}
		
		void Update () {
			
		}
	}
}