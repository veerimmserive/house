# Veer: House #

House is a prototype architectural CAD for VR. This section is too short and time is too precious so just read the spec here: [insert link here]().

## What is this repository for? ##

* Version: 0.0.1 (MVP)

## How do I get set up? ##

### Minimum hardware specs to run this:

```
CPU: Intel i5-2500K
GPU: Nvidia GTX 970 

```


### Configuration

None, as of this moment.


### Dependencies

None, as of this moment.


### How to run tests

Tests are much harder to do in VR apps, so we're doing without them here.


### Deployment instructions

None.

## Contribution guidelines ##

* See technical spec here: [insert link here]()

## Who do I talk to? ##

Send your concerns to anyone on the [Veer Immersive team](https://bitbucket.org/veerimmserive/) or better yet, open an [issue](https://bitbucket.org/zrkrlc/house/issues?status=new&status=open).